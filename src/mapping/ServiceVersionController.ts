/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Controller for managing all operations of the Service-Owner
 */

import  logger                          from "../utils/Logger";
import {PostRespBusinessObjects}        from "./bussObj/Response";
import OracleDBInteractionUtil          from "../utils/OracleDBInteractionUtil";
import util                             from "util";
import AppConfig                        from "../config/AppConfig";
import { response } from "express";

export default class ServiceVersionController{
    private readonly _MAX_RECORD_ABUSE:number = 100;    
    
    /**
     * Creates service-version in the underlying persistent layer
     * @param reqJSON (JSON) object for creating the service-version
     */
    public async create(reqJSON:any){
        logger.info(`Creating service-owner entitiy in the data store`);
        logger.debug(`Request received for creating service-version: ${util.inspect(reqJSON,{compact:true,colors:true, depth: null})} `);        
        var response!:string;        
        try{
            response = await this.createServiceVersion(reqJSON)
            logger.info(`Service created sucessfully`);
        }catch(error){         
            logger.error(`Error while creating service version: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8350","Error While performing the Operation");            
            }
            
        }    
        return response;    
    }

    /**
     * Update service-version in the underlying persistent layer
     * @param reqJSON (JSON) JSON object
     */
    public async update(reqJSON:any, origVerID:string, origSrvID:string){
        logger.info(`Creating service-owner entitiy in the data store`);
        logger.debug(`Request received for creating service-version: ${util.inspect(reqJSON,{compact:true,colors:true, depth: null})} `);        
        var response!:string;        
        try{
            response = await this.updateServiceVersion(reqJSON,origVerID,origSrvID)
            logger.info(`Service created sucessfully`);
        }catch(error){         
            logger.error(`Error while creating service owner: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8350","Error While performing the Operation");            
            }
        }    
        return response;                            
    }

    public async delete(verID:string, srvID:string){
        logger.info(`Creating service-owner entitiy in the data store`);
        logger.debug(`Request received for deleting service-version with ver_id ${verID} and srv_id ${srvID}`);        
        var response!:string;        
        try{
            response = await this.deleteServiceVersion(verID,srvID);
            logger.info(`Service deleted sucessfully`);            
        }catch(error){         
            logger.error(`Error while creating service owner: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            if(error.metadata){ throw error;}            
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8350","Error While performing the Operation");            
            }
        }   
        return response;
    }
    

    /**
     * Get all met-content of the service.
     * NOTE: from and to determine the 
     * @param from (number) From record number
     * @param to (number) To record number
     * @async
     */
    public async getAll(from:number=1,to:number=10){
        logger.info(`Getting all service-version record from the db starting from: ${from} - to - ${to}`);
        to = (to > this._MAX_RECORD_ABUSE)? this._MAX_RECORD_ABUSE: to;                    
        if(from > to){
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8355","Invalid input");
        }
        let sql:string = `select RES.*,(SELECT COUNT(*) FROM med_service_versions) as TTL_ROWS from (
            SELECT ROWNUM,version_id,srv_id,description,container_reg_uri,container_reg_secid,created_on,updated_on,created_by,updated_by,last_ops_id FROM med_service_versions
            ) RES where ROWNUM BETWEEN :v1 and :v2 order by rownum `;
        let binds= [from,to];
        try{
            let result = await OracleDBInteractionUtil.executeRead(sql, binds);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            let busObj = await this.createBusinessObjectForAll(result);
            return busObj;
        }catch(error){
            logger.error(`Error occured while converting data to business object ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            return error;
        }
    }

    /**
     * Get a specific service from the data store
     * @param srvID (string) Service ID to get the version of
     */
    public async get(srvID:string){
        logger.info(`Getting all service-version record from the db for a specific service ${srvID}`);        
        let sql:string = `SELECT ROWNUM,version_id,srv_id,description,container_reg_uri,container_reg_secid,created_on,updated_on,created_by,updated_by,last_ops_id FROM med_service_versions WHERE srv_id=:v1`;
        let binds= [srvID];
        try{
            let result = await OracleDBInteractionUtil.executeRead(sql, binds);
            logger.debug(`Result: ${typeof result} object:  ${util.inspect(result,{compact:true,colors:true, depth: null})}`);     
            let busObj = await this.createBusinessObjectForAll(result);
            return busObj;
        }catch(error){
            logger.error(`Error occured while converting data to business object ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            return error;
        }
    }



    private async createServiceVersion(reqJSON:any){
        let srvVersionObj = reqJSON['service-version'];
        let sqlIns:string = `INSERT INTO med_service_versions (version_id,srv_id,description,container_reg_uri,container_reg_secid,created_by,updated_by,last_ops_id) VALUES (:v0,:v1,:v2,:v3,:v4,:v5,:v6,:v7)`;
        let bindIns = [srvVersionObj.version_id,srvVersionObj.srv_id,srvVersionObj.description,srvVersionObj.container_reg_uri,srvVersionObj.container_reg_secid, 'SYSTEM','SYSTEM',null];        
        try{
            let selResp = await OracleDBInteractionUtil.executeRead(`SELECT version_id FROM med_service_versions WHERE version_id=:v1 AND srv_id=:v2`, [srvVersionObj.version_id, srvVersionObj.srv_id]);
            if(selResp.rows.length > 0){            
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8354","Data already exist");
            }                                       
            let result = await OracleDBInteractionUtil.executeUpdate(sqlIns,bindIns);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        }catch(error){
            logger.error(`Error while creating service owner: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}            
            if(AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8350","Error While performing the Operation", new PostRespBusinessObjects.Trace("DB_ERROR", `${util.inspect(error,{compact:true,colors:true, depth: null})}`));
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8350","Error While performing the Operation");
            }
        }
    }

    private async updateServiceVersion(reqJSON:any, origVerID:string, origSrvID:string){
        // Update service version
        let srvVersionObj = reqJSON['service-version'];
        try{
            let selResp = await OracleDBInteractionUtil.executeRead(`SELECT version_id FROM med_service_versions WHERE version_id=:v1 AND srv_id=:v2`, [srvVersionObj.version_id, srvVersionObj.srv_id]);
            if(selResp.rows.length > 0){            
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8354","Data already exist");
            }    
            let sqlUpd:string = `UPDATE med_service_versions SET version_id=COALESCE(:v0,version_id),srv_id= COALESCE(:v1,srv_id),description = COALESCE(:v2,description), container_reg_uri = COALESCE(:v3,container_reg_uri), container_reg_secid = COALESCE(:v4,container_reg_secid), updated_on = CURRENT_TIMESTAMP, updated_by = COALESCE(:v5,updated_by), last_ops_id = COALESCE(:v6,last_ops_id) WHERE version_id = :v7 AND srv_id= :v8`;
            let bindUpd = [srvVersionObj.version_id,srvVersionObj.srv_id,srvVersionObj.description,srvVersionObj.container_reg_uri,srvVersionObj.container_reg_secid, 'SYSTEM',null, origVerID, origSrvID];                                           
            let result = await OracleDBInteractionUtil.executeUpdate(sqlUpd,bindUpd);
            if(result.rowsAffected <= 0){
                logger.error(`Update failed to chagne rows in the database.`)
                logger.debug(`Resposne received from the database: ${util.inspect(result,{compact:true,colors:true, depth: null})}`);
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8353","Required key does not exist in the db");
            }else{
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
            }                        
        }catch(error){
            logger.error(`Error while updating service owner: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}    
            if(AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8350","Error While performing the Operation", new PostRespBusinessObjects.Trace("DB_ERROR", `${util.inspect(error,{compact:true,colors:true, depth: null})}`));
            }else{
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8350","Error While performing the Operation");
            }
        }        
    }

    private async deleteServiceVersion(verID:string, srvID: string){
        try{
            let selResp = await OracleDBInteractionUtil.executeRead(`SELECT version_id FROM med_service_versions WHERE version_id=:v1 AND srv_id=:v2`, [verID,srvID]);
            if(selResp.rows.length <= 0){            
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8353","Required key does not exist in the db");
            }
            let delResp =await OracleDBInteractionUtil.executeUpdate(`DELETE FROM med_service_versions WHERE version_id=:v1 AND srv_id=:v2`, [verID,srvID]);
            if(delResp.rowsAffected <= 0){
                logger.error(`Delete failed to chagne rows in the database.`)
                logger.debug(`Resposne received from the database: ${util.inspect(delResp,{compact:true,colors:true, depth: null})}`);
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8353","Required key does not exist in the db");
            }else{
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
            }                        
        }catch(error){
            logger.error(`Error while updating service owner: ${util.inspect(error,{compact:true,colors:true, depth: null})}`)            
            if(error.metadata){ throw error;}    
            if(AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8350","Error While performing the Operation", new PostRespBusinessObjects.Trace("DB_ERROR", `${util.inspect(error,{compact:true,colors:true, depth: null})}`));
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8350","Error While performing the Operation");
            }
        }        
    }

    private async createBusinessObjectForAll(dbResult:any){
        return new Promise<any>((resolve:any,reject:any) => {
            if(dbResult.rows.length <= 0){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("8353","Required key does not exist in the db"));
            }
            var bussObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),
                    "trace": []
                },
                "service-version": []
            }; 
            if(AppConfig.config.OFL_MED_NODE_ENV !== undefined
                && AppConfig.config.OFL_MED_NODE_ENV.toLowerCase() === 'debug'
                    && dbResult.rows[0].TTL_ROWS){
                bussObj.metadata['trace'].push(
                    {
                        "source": "TotalRows",
                        "description": dbResult.rows[0].TTL_ROWS
                    }
                );
            }                   
            for(let i:number =0; i< dbResult.rows.length; i++){
                bussObj["service-version"].push({ 
                    "version_id": dbResult.rows[i].VERSION_ID,                    
                    "srv_id": dbResult.rows[i].SRV_ID,
                    "description": dbResult.rows[i].DESCRIPTION,
                    "container_reg_uri": dbResult.rows[i].CONTAINER_REG_URI,
                    "container_reg_secid": dbResult.rows[i].CONTAINER_REG_SECID,
                    "created_on": dbResult.rows[i].CREATED_ON,
                    "updated_on": dbResult.rows[i].UPDATED_ON,
                    "created_by": dbResult.rows[i].CREATED_BY,
                    "updated_by": dbResult.rows[i].UPDATED_BY,
                    "last_ops_id": dbResult.rows[i].LAST_OPS_ID
                });
            }
            resolve(bussObj);
        });
    }

}