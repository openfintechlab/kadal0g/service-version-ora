/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Validator service for validating service-version JSON objects
 */

import Joi                              from "joi";
import  logger                          from "../../utils/Logger";
import {PostRespBusinessObjects}        from "../bussObj/Response";



export default class ServiceVersionSchemaModel{
    ServiceVersionReqSchema_CRT = Joi.object({
        "service-version":{
            "version_id":Joi.string().min(8).max(56).required(),
            "srv_id": Joi.string().min(8).max(56).required(),
            "description": Joi.string().min(8).max(128).required(),
            "container_reg_uri": Joi.string().min(4).max(256).required(),
            "container_reg_secid": Joi.string().max(16),
            "created_on": Joi.date().iso(),
            "updated_on": Joi.date().iso(),
            "created_by": Joi.string().min(3).max(64),
            "updated_by": Joi.string().min(3).max(64),
            "last_ops_id": Joi.string().min(3).max(56)
         }
    });

  

    /**
     * Validates the request object agains the pre-defined structure model
     * @param req (any) Request Object to validate
     */
    public validateCreateRequest(req:any){
        const {error,value} = this.ServiceVersionReqSchema_CRT.validate(req);
        if(error){
            logger.error(`Validation error ${error}`);            
            const trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8352","Schema validation error",trace);                                    
        }else{            
            return value;
        }
    }
    
}

/**
 * 
 * CREATE TABLE med_service_versions (
    version_id           VARCHAR2(56 CHAR) NOT NULL,
    srv_id               VARCHAR2(56 CHAR) NOT NULL,
    description          VARCHAR2(128 BYTE) NOT NULL,
    container_reg_uri    VARCHAR2(256 CHAR) NOT NULL,
    container_reg_secid  VARCHAR2(16 CHAR),
    created_on           TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    updated_on           TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    created_by           VARCHAR2(64) NOT NULL,
    updated_by           VARCHAR2(64),
    last_ops_id          VARCHAR2(56 CHAR)
);

 */