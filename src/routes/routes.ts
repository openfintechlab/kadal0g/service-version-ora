/**
 * Copyright 2020-2022 Openfintechlab, Inc. All rights reserved.
 * Licenses: LICENSE.md
 * Description:
 * Root enry level file forr bootstarting node js application
 */

import express                          from "express";
import {AppHealth}                      from "../config/AppConfig";
import logger                           from "../utils/Logger";
import ServiceVersionSchemaModel        from "../mapping/validators/ServiceVersionGBO.validator";
import util                             from "util";
import ServiceVersionController         from "../mapping/ServiceVersionController";
import {PostRespBusinessObjects}        from "../mapping/bussObj/Response";


const router: any = express.Router();

/**
 * Creates service-version entitiy in the database
 */
router.post('/',async (request:any, response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for creating service-version`);
    try{
        let parsedObj:any = await new ServiceVersionSchemaModel().validateCreateRequest(request.body);
        logger.debug(`Parsed response from validation: ${util.inspect(parsedObj.metadata,{compact:true,colors:true, depth: null})}`)
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(500);
            response.send(parsedObj)
        }else{
            // Call service controller
            // TODO! Call DButil for creating the sepcific object            
            let respObj = await new ServiceVersionController().create(parsedObj);
            response.status(201);
            response.send(respObj);
        }        
    }catch(error){
        logger.debug(`Error occured while creating service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        if(error.metadata){            
            response.send(error);
        }else{            
            response.send(new PostRespBusinessObjects.PostingResponse().generateBasicResponseJSON("9901","Error while processing the request"));
        }     
    }    
});

/**
 * Updates the service-version entity in the database
 */
router.put('/:verID/:srvID', async (request:any, response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for updating service-version`);
    try{        
        // NOTE: validation for create and update will remain same. We will take original values from the URL        
        let parsedObj:any = await new ServiceVersionSchemaModel().validateCreateRequest(request.body); 
        logger.debug(`Parsed response from validation: ${util.inspect(parsedObj.metadata,{compact:true,colors:true, depth: null})}`)
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(500);
            response.send(parsedObj)
        }else{
            // Call service controller
            // TODO! Call DButil for creating the sepcific object            
            let respObj = await new ServiceVersionController().update(parsedObj,request.params.verID,request.params.srvID); 
            response.status(201);
            response.send(respObj);// <<-- CHANGE IT!
        }                        

    }catch(error){
        logger.debug(`Error occured while creating service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        response.send(error);
    }    
});

/**
 * Delete service version
 */
router.delete('/:verID/:srvID',async (request:any, response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for deleting service-version`);
    try{
        let respObj = await new ServiceVersionController().delete(request.params.verID,request.params.srvID); 
        response.status(201);
        response.send(respObj);
    }catch(error){
        logger.error(`Error occured while creating service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
        response.status(500);
        response.send(error);
    }    
    
});


/**
 * Get all service-versions
 */
router.get('/', async (request:any, response:any) => {
    response.set("Content-Type","application/json; charset=utf-8");    
    logger.info(`Procedure called for getting all service-version between specific range`);
    let from:number, to:number;
    from = (request.query.from)? request.query.from : undefined;
    to = (request.query.to)? request.query.to : undefined;
    try{
        let parsedObj = await new ServiceVersionController().getAll(from,to);
        logger.debug(`Parsed response from validation: ${util.inspect(parsedObj.metadata,{compact:true,colors:true, depth: null})}`)
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(404);
            response.send(parsedObj)
        }else{
            response.set(200);
            response.send(parsedObj);
        }        
    }catch(error){
        logger.error(`Error occured while creating service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)        
        response.status(500);
        response.send(error);
    }    
});

/**
 * Get a specific service-version using service-id
 * 
 */
router.get('/:srvID', async (request:any, response:any) => {
    try{
        response.set("Content-Type","application/json; charset=utf-8");    
        logger.info(`Procedure called for geting all service-versions using service ID: ${request.params.srvID}`);
        let parsedObj = await new ServiceVersionController().get(request.params.srvID);
        logger.debug(`Parsed response from validation: ${util.inspect(parsedObj.metadata,{compact:true,colors:true, depth: null})}`)
        if(parsedObj.metadata && parsedObj.metadata.status && parsedObj.metadata.status !== '0000'){
            response.status(404);
            response.send(parsedObj)
        }else{
            response.set(200);
            response.send(parsedObj);
        }    
    }catch(error){
        logger.error(`Error occured while creating service-version:  ${util.inspect(error,{compact:true,colors:true, depth: null})}`)        
        response.status(500);
        response.send(error);
    }
});



export default router;